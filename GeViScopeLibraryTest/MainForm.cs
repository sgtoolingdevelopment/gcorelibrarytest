﻿using GCoreLibrary;
using System;
using System.Windows.Forms;

namespace GCoreLibraryTest
{
    public partial class MainForm : Form
    {
        private GCoreForm gCoreFormForm;

        public MainForm()
        {
            InitializeComponent();
            gCoreFormForm = new GCoreForm(Handle);
        }

        private void ButtonCameraSelect_Click(object sender, EventArgs e)
        {
            gCoreFormForm.Show();
            gCoreFormForm.WindowState = FormWindowState.Normal;
            gCoreFormForm.BringToFront();
        }

        private void BtnTakeSnapshot_Click(object sender, EventArgs e)
        {
            gCoreFormForm.ShowMessage("VERIFY SNAPSHOT...");
            gCoreFormForm.TakeSnapshot("This is a comment");
        }

        private void BtnSaveSnapshot_Click(object sender, EventArgs e)
        {
            String snapShotFile = "C:" + @"\\Users" + @"\Public" + @"\Documents" + @"\GCoreLibraryTest" + "-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + "-" + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".bmp";
            //snapshotForm.SaveSnapshot("C://Users/Public/Documents/test.bmp");
            //snapshotForm.SaveSnapshot(@"C:\\Users\Public\Documents\test.bmp");
            gCoreFormForm.SaveSnapshot(snapShotFile);
        }

        private void BtnCancelSnapshot_Click(object sender, EventArgs e)
        {
            gCoreFormForm.ShowVideo();
            gCoreFormForm.CancelSnapshot();
        }

        private void BtnExitSnapshot_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show(this, "EXIT SNAPSHOT TEST", "SNAPSHOT TEST", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialogResult == DialogResult.Yes)
            {
                gCoreFormForm.Close();

                // Exit the application (close windows down)
                Application.Exit();
            }
        }

        private void MainForm_Click(object sender, EventArgs e)
        {
            gCoreFormForm.Close();

            // Exit the application (close windows down)
            Application.Exit();
        }
    }
}
